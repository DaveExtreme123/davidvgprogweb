<?php

$alphabet = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
$cle = "SANDWISH";
$str = file("cle.txt");
$motCour = preg_split('/[;]+/',$str);
for ($i = 0; $i < count($motCour); $i++) {
    echo $motCour[$i];
}
if(isset($_POST['premierTexte'])) {
    @$texteModifier = encrypter($alphabet,$cle,$_POST['premierTexte']);

}
if(isset($_POST['deuxiemeTexte'])){
    @$texte2Modifier = decrypter($alphabet,$cle,$_POST['deuxiemeTexte']);
}

function encrypter($alphabet,$cle,$value){
    $tableauCle = str_split($cle);
    $nouvTableau = str_split(strtoupper($value));
    $nouveauMessage = "";
    $j = 0;

  for($i = 0; $i < count($nouvTableau); $i++ ){

      if(ctype_alpha($nouvTableau[$i])) {
          $position = (trouverLettre($alphabet,$nouvTableau[$i]) + trouverLettre($alphabet,@$tableauCle[$j])) % 26;
          if($position > 26){
              $position -= 26;
          }
          $lettre = $alphabet[$position];
      }else{
          $lettre = $nouvTableau[$i];
      }

      $nouveauMessage = $nouveauMessage . $lettre;

      if ($j < count($tableauCle)){
          $j++;
      }else{
          $j = 0;
      }
  }
    return $nouveauMessage;

}

function decrypter($alphabet,$cle,$value){
    $tableauCle = str_split($cle);
    $nouvTableau = str_split(strtoupper($value));
    $nouveauMessage = "";
    $j = 0;

    for($i = 0; $i < count($nouvTableau); $i++ ){

        if(ctype_alpha($nouvTableau[$i])) {
            $position = (trouverLettre($alphabet,$nouvTableau[$i]) - trouverLettre($alphabet,@$tableauCle[$j])) % 26;
            if($position < 0){
                $position += 26;
            }
            $lettre = $alphabet[$position];
        }else{
            $lettre = $nouvTableau[$i];
        }

        $nouveauMessage = $nouveauMessage . $lettre;

        if ($j < count($tableauCle)){
            $j++;
        }else{
            $j = 0;
        }
    }
    return $nouveauMessage;
}

function trouverLettre($alphabet,$lettre){
    for($i = 0; $i < count($alphabet);$i++){
        if($lettre == $alphabet[$i]){
            $position = $i;
        }
    }

    return $position;
}
require_once "Vue/VueChange.php";