<?php
//route selon l'action choisie
if(@$_POST['liste'])
    //page des étudiants par programme
    require_once 'Controleur/Affiche.php';
else if(@$_POST['detail'])
    //page du détail de chaque étudiant
    require_once 'Controleur/Detail.php';
else if(@$_POST['quitter'])
    //on quitte l'application
    require_once 'Controleur/Quitter.php';
else
    //point de départ
    require_once 'Controleur/Accueil.php';
