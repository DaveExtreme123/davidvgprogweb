<?php

/** Classe pour l'information d'un élève permettant de stocker l'information
 *  provenant de la BD
 */
class Eleve
{

    private $id,
            $nom,
            $prenom,
            $moyenne;

    /**
     * Eleve constructor.
     * @param $id
     * @param $nom
     * @param $prenom
     * @param $moyenne
     */
    public function __construct($id, $nom, $prenom, $moyenne)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->moyenne = $moyenne;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getMoyenne()
    {
        return $this->moyenne;
    }

    /**
     * @param mixed $moyenne
     */
    public function setMoyenne($moyenne): void
    {
        $this->moyenne = $moyenne;
    }

    public function toString(){
        return $this->getNom() . " , " . $this->getPrenom()  . "," . $this->getMoyenne();
    }


}