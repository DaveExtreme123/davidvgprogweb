<?php


class Programme
{

    private $id;
    private $nom;
    private $credit;

    /**
     * Programme constructor.
     * @param $id
     * @param $nom
     * @param $credit
     */


    public function __construct($id, $nom, $credit)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->credit = $credit;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @param mixed $credit
     */
    public function setCredit($credit): void
    {
        $this->credit = $credit;
    }

    public function toString(){
        return 'test';
    }

}