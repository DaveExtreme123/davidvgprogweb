<?php
require_once "Modele/Eleve.php";
require_once "Modele/Programme.php";
class Bdd
{
    private $bdd;


    /**
     * Constructeur de la classe
     * Connexion aux serveur de base de donnée et sélection de la base
     *
     */
    public function __construct($serveur = 'localhost', $bdd = 'tp3b', $identifiant = 'root', $motDePasse = '')
    {
        try {
            $dsn = 'mysql:host=' . $serveur . ';dbname=' . $bdd . ";'";
            $this->bdd = new PDO($dsn, $identifiant, $motDePasse);
            //construire l'objet Pdo
            

            //pour avoir les accents
            $this->bdd->exec("set names utf8");
            $this->requetePrepare = "";
        }
        catch(PDOException $e){
            throw new PDOException("Erreur de connexion avec la base de données", 100);
        }

    }

    private function preparer($chaineSQL)
    {
        return $this->requetePrepare = $this->bdd->prepare($chaineSQL);

    }

    private function executer($paramNomme, $param)
    {
        $this->requetePrepare->execute(array($paramNomme => $param));
        if (!$this->requetePrepare) {
            throw new PDOExeption('Erreur de requête SQL');
        }
    }


    public function fermerConnexion()
    {

       $this->bdd = null;

    }


    /* Retourne le résultat de la requête des élèves par programme
        On reçoit quel programme en paramètre
        Utiliser une requête préparée avec un marqueur nommé
       $param représente l'id du programme
       @return $tableauEleve le tableau d'objets Eleve pour le programme demandé
    */
    public function getElevesParProgramme($param){
        $tableau = array();
        $paramNomme = ':NomProgramme';
        $chaineSQL = "SELECT * FROM Eleve WHERE Programme = " . $paramNomme;
        $this->requetePrepare = $this->preparer($chaineSQL);
        $this->executer($paramNomme, $param);
        while ($ligne = $this->requetePrepare->fetch(PDO::FETCH_OBJ)) {
            $eleve = new Eleve($ligne->IdEtudiant, $ligne->NomEtudiant, $ligne->PrenomEtudiant,$ligne->MoyenneGenerale);
            array_push($tableau, $eleve);
        }

        return $tableau;
    }

    /*Retourne la meilleure moyenne générale pour le programme demandé
        On reçoit quel programme en paramètre
        Utiliser une requête préparée avec un marqueur nommé
       $param représente l'id du programme
       @return $meilleure est la note de la meilleure moyenne générale
     *
     */
    public function getEleveMeilleureMoyenne($param){
        $meilleure = 0;
        $tableau = array();
        $paramNomme = ':NomProgramme';
        $chaineSQL = "SELECT * FROM Eleve WHERE Programme = " . $paramNomme;
        $this->requetePrepare = $this->preparer($chaineSQL);
        $this->executer($paramNomme, $param);
        while ($ligne = $this->requetePrepare->fetch(PDO::FETCH_OBJ)) {
            $eleve = new Eleve($ligne->IdEtudiant, $ligne->NomEtudiant, $ligne->PrenomEtudiant,$ligne->MoyenneGenerale);
            array_push($tableau, $eleve);
          if($meilleure < $ligne->MoyenneGenerale){
              $meilleure = $ligne->MoyenneGenerale;
          }
        }
       return $meilleure;





    }

    /* Retourne le résultat d'une requète permettant d'obtenir
     * un tableau de l'ensemble des programmes
     * @return $tableauProgramme, tous les objets Programme de la table Programme
     */
    public function getProgrammes(){


    }

    /*
     * Retourne l'information détaillée sur le programme demandé
     * On reçoit quel programme en paramètre
        Utiliser une requête préparée avec un marqueur nommé
       $param représente l'id du programme
       @return @monProgramme un objet Programme (à construire) ou l'id du programme et son nom sous forme de chaîne
     */
    public function getProgramme($param){
        $monProgramme = new Programme("","",0);
        $paramNomme = ':NomProgramme';
        $chaineSQL = "SELECT * FROM Programme WHERE IdProgramme = " . $paramNomme;
        $this->requetePrepare = $this->preparer($chaineSQL);
        $this->executer($paramNomme, $param);
        $ligne = $this->requetePrepare->fetch(PDO::FETCH_OBJ);
        $monProgramme = new Programme($ligne->IdProgramme, $ligne->NomProgramme, $ligne->NbCredits);

        return $monProgramme;

    }
}
