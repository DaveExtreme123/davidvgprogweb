<?php
require_once "Modele/Bdd.php";

$param = $_POST['programme'];
$maBd = new Bdd();
/*faire les traitements nécessaires dans BD
 *pour obtenir le tableau des élèves pour le programme demandé
 * la meilleureMoyenne
 * et l'information sur le programme
 * et éventuellement conserver la persistance de cette information
 *
 */
$maBd->getProgramme($param);
$tabEleve = $maBd->getElevesParProgramme($param);
$meilleurMoyenne = $maBd->getEleveMeilleureMoyenne($param);



//afficher la vue
require_once 'Vue/EnTete.php';
require_once "Vue/VueAffiche.php";
