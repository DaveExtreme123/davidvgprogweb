-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour tp3b
DROP DATABASE IF EXISTS `tp3b`;
CREATE DATABASE IF NOT EXISTS `tp3b` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tp3b`;

-- Listage de la structure de la table tp3b. programme
DROP TABLE IF EXISTS `programme`;
CREATE TABLE IF NOT EXISTS `programme` (
  `IdProgramme` varchar(3) NOT NULL,
  `NomProgramme` varchar(50) NOT NULL,
  `NbCredits` int(11) NOT NULL,
  PRIMARY KEY (`IdProgramme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table tp3b.programme : ~5 rows (environ)
/*!40000 ALTER TABLE `programme` DISABLE KEYS */;
INSERT IGNORE INTO `programme` (`IdProgramme`, `NomProgramme`, `NbCredits`) VALUES
	('200', 'Sciences informatiques et mathématiques', 96),
	('241', 'Génie mécanique', 90),
	('300', 'Sciences humaines', 96),
	('410', 'Administation', 90),
	('420', 'Informatique', 90);
/*!40000 ALTER TABLE `programme` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


-- Listage de la structure de la table tp3b. eleve
DROP TABLE IF EXISTS `eleve`;
CREATE TABLE IF NOT EXISTS `eleve` (
  `IdEtudiant` bigint(20) NOT NULL AUTO_INCREMENT,
  `NomEtudiant` varchar(30) NOT NULL,
  `PrenomEtudiant` varchar(30) NOT NULL,
  `Programme` varchar(3) NOT NULL,
  `MoyenneGenerale` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`IdEtudiant`),
  KEY `Programme` (`Programme`),
  CONSTRAINT `eleve_ibfk_1` FOREIGN KEY (`Programme`) REFERENCES `programme` (`IdProgramme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table tp3b.eleve : ~20 rows (environ)
/*!40000 ALTER TABLE `eleve` DISABLE KEYS */;
INSERT IGNORE INTO `eleve` (`IdEtudiant`, `NomEtudiant`, `PrenomEtudiant`, `Programme`, `MoyenneGenerale`) VALUES
	(100, 'Bethany Dickens I', 'Emelie', '200', 65.90),
	(101, 'Joy Bauch', 'Keven', '410', 88.75),
	(102, 'Jeremy Smith', 'Sedrick', '300', 68.75),
	(103, 'Kaci Stark IV', 'Jermey', '200', 91.00),
	(104, 'Prof. Jackeline Witting II', 'Germaine', '241', 59.00),
	(105, 'Mrs. Kristina Beier Sr.', 'Elinore', '300', 58.50),
	(106, 'Lucas Klocko', 'Esteban', '410', 59.00),
	(107, 'Mr. Jared Osinski PhD', 'Myah', '200', 45.00),
	(108, 'Danielle Goodwin III', 'Darryl', '241', 63.00),
	(109, 'Ezra Roob PhD', 'Jeff', '241', 92.80),
	(110, 'Miss Maggie Torphy', 'Hubert', '420', 94.00),
	(111, 'Merlin Tremblay', 'Keyon', '420', 97.00),
	(112, 'Mr. Foster Yost', 'Dexter', '200', 75.00),
	(113, 'Luisa Hayes', 'Tremayne', '300', 46.00),
	(114, 'Zoila Wiegand', 'Matilda', '410', 77.00),
	(115, 'Dr. Dion Wolff V', 'Price', '420', 71.00),
	(116, 'Ms. Brianne Altenwerth', 'Myrna', '300', 78.00),
	(117, 'Leslie Kassulke IV', 'Lisa', '410', 82.00),
	(118, 'Giovanny Toy Sr.', 'Tracey', '200', 39.00),
	(119, 'Orlando Swaniawski', 'Queen', '241', 60.00);
/*!40000 ALTER TABLE `eleve` ENABLE KEYS */;


